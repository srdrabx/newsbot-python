class News:
    title = "Default Title. If you see this, there is an error."
    body = "Default Body. If you see this, there is an error."
    image = "Default image. This isn't even a link.png"
    adspace = ""
    tf = False
    def __init__(self, title, body, image, adspace, tf):
        self.title = title
        self.body = body
        self.image = image
        self.adspace = adspace
        self.tf = tf
    def return_message(self):
        if self.adspace != "":
            return ( [BR MOTD] + "\n" + self.adspace + "\n" + self.title + "\n" + self.body)
        else:
            return ([BR MOTD] + "\n" + self.title + "\n" + self.body + "\n")
    def set_tf(self, bool):
        self.tf = bool