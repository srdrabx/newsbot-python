import tweepy
from twisted.internet import task, reactor
import urllib.request
from news import News
import requests
import os

consumer_key = 'CONSUMER_KEY_HERE'
consumer_secret = 'CONSUMER_SECRET_HERE'
access_token = 'ACCESS_TOKEN_HERE'
access_token_secret = 'ACCESS_TOKEN_HERE'
timeout = 60.0 #Seconds to next request

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

def send_tweet(api):
    global newsdict
    response = urllib.request.urlopen('https://lazylinks.fr/api/news')
    fulltext = str(response.read())
    file = open("savedtext.txt", "w")
    file.write(fulltext)
    file.close()
    newsdict = filldict(fulltext, False, newsdict)
    for item in newsdict:
        if newsdict[item].tf == False:
            message = newsdict[item].return_message()
            print(message)
            filename = 'temp.jpg'
            request = requests.get(newsdict[item].image, stream=True)
            if request.status_code == 200:
                with open(filename, 'wb') as image:
                    for chunk in request:
                        image.write(chunk)
                api.update_with_media(filename, status=message)
                os.remove(filename)
            else:
                print("Unable to download image")
            newsdict[item].set_tf(True)
    pass

def filldict(text, tf, newsdict):
    newstext = text.split('},')
    for item in newstext:
        image = item.split('"image": "')[1].split('",')[0]
        title = item.split('"title": "')[1].split('",')[0]
        body = item.split('"body": "')[1].split('"')[0]
        adspace = ""
        if '"adspace":' in item:
            adspace = item.split('"adspace": "')[1].split('"')[0]
        news = News(title, body, image, adspace, tf)
        id = title+body+image+adspace
        if id not in newsdict:
            newsdict[id] = news
    return newsdict

newsdict = {}
try:
    savedtext  = open("savedtext.txt", "r")
    text = savedtext.read()
    newsdict = filldict(text, True, newsdict)
    savedtext.close()
except:
    print("No saved data.")


l = task.LoopingCall(send_tweet, api)
l.start(timeout) # call every sixty seconds
reactor.run()
