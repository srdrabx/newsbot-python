# Fortnite news bot python


Bot made in Python 3.6 to publish Fortnite : Battle Royale news on twitter.

(The bot works the same way with STW news, just change the endpoint.)

**Required**

Twitter API Access

Python 3.6

Pip

 - Tweetpy 	 
 - Twisted 	 
 - urllib.request

 API endpoint (https://github.com/Tresmos/fortnite-webapi)

**Installation**

 1. Go to https://developer.twitter.com/en/apps and create an app.
 2. Fill in the required fields and create it.
 3. Then go to the APP, go to "Key and Tokens" and generate the "Access token & access token secret".
 4. Copy each token into main.py, replacing the TOKEN_HERE
 5. Install the requirements, search on Google if you don't know how.
 6. Start the bot with `python3 main.py`
 7. If it doesn't work, check the error, it could be because of these options.
 
		-There are not enough permissions

		-You don't have the minimum requirements installed
		
		-You have installed another version of Python that is not 3.6
		
		-The server where it is hosted cannot run it, e.g. Heroku.
		
# The endpoint https://lazylinks.fr/api/news, will not work, you will have to create one yourselves.
